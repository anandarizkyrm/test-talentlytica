import { useMemo, useState } from "react";
import "./App.css";
import ListMahasiswa from "./components/ListMahasiswa";

function App() {
  const [dataPenilaian, setDataPenilaian] = useState<unknown>({
    aspek_penilaian_1: {},
    aspek_penilaian_2: {},
    aspek_penilaian_3: {},
    aspek_penilaian_4: {},
  });

  const [outputData, setOutputdata] = useState<unknown>();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setOutputdata(dataPenilaian);
  };

  const memoizedOutputData = useMemo(() => {
    return JSON.stringify(outputData, null, 2);
  }, [outputData]);
  return (
    <>
      <div className="title-container">
        <p>Aspek Penilaian 1</p>
        <p>Aspek Penilaian 2</p>
        <p>Aspek Penilaian 3</p>
        <p>Aspek Penilaian 4</p>
      </div>

      <form onSubmit={(e) => handleSubmit(e)}>
        <div className="list-container">
          {[...Array(10)].map((_, idx) => (
            <ListMahasiswa
              key={idx}
              displayName={`Mahasiswa ${idx + 1}`}
              name={`mahasiswa_${idx + 1}`}
              setData={setDataPenilaian}
            />
          ))}
        </div>
        <button type="submit">Simpan</button>
      </form>
      <div className="json">
        <pre>{memoizedOutputData}</pre>
      </div>
    </>
  );
}

export default App;
