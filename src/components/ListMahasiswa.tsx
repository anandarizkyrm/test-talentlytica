import React from "react";
import { Avatar } from "./Avatar";

type Props = {
  name: string;
  displayName: string;
  setData: React.Dispatch<React.SetStateAction<unknown>>;
};

const ListMahasiswa = ({ name, setData, displayName }: Props) => {
  const handleChange = (
    e: React.ChangeEvent<HTMLSelectElement>,
    selectName: string
  ) => {
    setData((prevState: any) => {
      // create a copy of the previous state using object spread syntax
      const newData = { ...prevState };

      // update the value of the select box
      newData[selectName][name] = e.target.value;

      // return the updated state
      return newData;
    });
  };

  return (
    <div className="flex flex-gap list">
      <Avatar />
      <h4>{displayName}</h4>
      {[...Array(4)].map((_, idx) => (
        <select
          key={idx}
          onChange={(e) => handleChange(e, `aspek_penilaian_${idx + 1}`)}
          name={`aspek_penilaian_${idx + 1}`}
        >
          <option value={""}>-</option>
          {[...Array(10)].map((_, idx) => (
            <option key={idx} value={idx + 1}>
              {idx + 1}
            </option>
          ))}
        </select>
      ))}
    </div>
  );
};

export default ListMahasiswa;
