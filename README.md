## HOW TO RUN ?

Access the project directory.

```bash
cd my-app
```

Install dependencies.

```bash
pnpm install
```

Serve with hot reload at <http://localhost:5173>.

```bash
pnpm run dev
```

## DEMO APP

### Tampilan Awal

![Screenshot from 2023-05-08 16-39-13](https://user-images.githubusercontent.com/79091335/236791068-0b01da08-3d77-40c0-b00e-f8e3ad8fd8cb.png)

### Tampilan on Button Simpan Clicked

![image](https://user-images.githubusercontent.com/79091335/236791422-78313fe5-8cdc-466d-89f0-a3f12c70f3b6.png)

### Overall Tampilan

![image](https://user-images.githubusercontent.com/79091335/236791535-1d11ff11-6de6-4e2d-9075-71595380b0f6.png)
